﻿// Module 18.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <algorithm>
template <class Type>
class Stack {
private:
    Type* elements;
    size_t currentIndex;
    size_t size;
    void Resize() {
        if (currentIndex < size - 1) {
            return;
        }
        Type* newElements = elements;
        elements = new Type[2 * size];
        for (int i = 0; i <= currentIndex; ++i) {
            elements[i] = newElements[i];
        }
        delete[] newElements;
    }
public:
    Stack() {
        elements = new Type[5];
        size = 5;
        currentIndex = -1;
    }
    void Push(Type element) {
        elements[++currentIndex] = element;
        Resize();
    }
    Type Pop() {
        return elements[currentIndex--];
    }
    size_t Size() {
        return currentIndex + 1;
    }
    size_t isEmpty() {
        return currentIndex == -1;
    }
};
template <class Type>
void Test(Type element) {
    Stack<Type>* tmp = new Stack<Type>();
    std::cout << tmp->isEmpty() << '\n';
    tmp->Push(element);
    std::cout << (tmp->Size() == 1) << '\n';
    std::cout << (tmp->Pop() == element) << '\n';
    for (int i = 0; i < 10; ++i) {
        tmp->Push(element);
    }
    std::cout << (tmp->Size() == 10) << '\n';

}
int main()
{
    std::cout << "Int Tests\n";
    Test(1);
    std::cout << "Float Tests\n";
    Test(1.1);
    std::cout << "Double Tests\n";
    Test(1.1111111111);
    std::cout << "String Tests\n";
    Test("abacaba");
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
