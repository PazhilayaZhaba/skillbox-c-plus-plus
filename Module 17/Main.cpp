#include <iostream>
#include <cmath>
#define PI 3.14
class Point {
public:
	Point()
	{}
	Point(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void ShowCoordinates() {
		std::cout << '\n' << x << " " << y << " " << z;
	}
private:
	double x = 0, y = 0, z = 0;
};

class Vector {
public:
	Vector()
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Show() {
		std::cout<< '\n' << x << " " << y << " " << z;
	}
	double Length() {
		return sqrt(x * x + y * y + z * z);
	}
private:
	double x = 0, y = 0, z = 0;
};

class Circle {
public:
	Circle()
	{}
	Circle(Point _center, double _radius) : center(_center), radius(_radius)
	{}
	double Length() {
		return 2 * PI * radius;
	}
	Point GetCenter() {
		return center;
	}
	double GetRadius() {
		return radius;
	}
	void ShowRadius() {
		std::cout <<'\n' << radius;
	}
	void ShowCenter() {
		center.ShowCoordinates();
	}
private:
	Point center;
	double radius = 0;
};

bool testDefaultVectorLength() {
	Vector tmp;
	return tmp.Length() == 0;
}

bool testCustomVectorLength(double result, double x, double y, double z) {
	Vector tmp(x, y, z);
	return result == tmp.Length();
}

bool testDefaultCircleLength() {
	Circle tmp;
	return 0 == tmp.Length();
}

bool testCustomCircleLength(double result, Point center, double radius) {
	Circle tmp(center, radius);
	return result == tmp.Length();
}

int main() {
	Point center(1, 1, 1);
	std::cout << testDefaultVectorLength() << '\n';
	std::cout << testCustomVectorLength(1, 1, 0, 0) << '\n';
	std::cout << testCustomVectorLength(1, 0, 1, 0) << '\n';
	std::cout << testCustomVectorLength(1, 0, 0, 1) << '\n';
	std::cout << testCustomVectorLength(5 ,0, 4, 3) << '\n';
	std::cout << testDefaultCircleLength() << '\n';
	std::cout << testCustomCircleLength(2 * PI, center, 1);
	center.ShowCoordinates();
	Circle tmp(center, 12);
	tmp.ShowCenter();
	tmp.ShowRadius();
}